/**
 * @file main.c
 * @brief Contains everything for Seal-Time Compression
 *
 * This file contains the programs
 * entrypoint (main()).
 *
 * @author SOFTWARE - A401
 * @version 1.0
 *
 * @section LICENSE
 * Distrubtion of this file or binary distrubtion is strictly prohibited,
 * unless explicit written consent is giving fron the author. To obtain
 * such consent, please contact us on email.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

#include "configuration.h"
#include "compression.h"
#include "settings.h"

#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif

#if !defined _DIRENT_HAVE_D_TYPE && defined HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif

#define STBI_ASSERT(x)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define TJE_IMPLEMENTATION
#include "tiny_jpeg.h"

#define MAX_SIGNATURES 10 /* Max number of signatures to load    */

/* Prototypes */
/**
 * @breif Loads all magicnumbers.
 * @param [out] filesignature A point to an array of magicnumbers whereto the file should read in to.
 * @return A uint containing the number of loaded magicnumbers.
 */
const uint load_magicnumbers(magicnumber *filesignature);

/**
 * @breif Checks if file type is known, based on the file signature.
 * @param [in] file The file to have its file type identified.
 * @param [in] filesignatures The list of known file signatures.
 * @param [in] num_of_signatures The number of known file signatures.
 * @return The "struct magicnumber" containing the file signature. NULL if file signature is not known.
 */
const magicnumber *analyzedata(FILE *file, const magicnumber filesignatures[], uint num_of_signatures, settings config);

/**
 * @breif Checks if file type is known, based on the file signature.
 * @param [in] header The first 10 characters from a file. The header contains the file signature.
 * @param [in] headerlen The number of characters in "header"
 * @param [in] filesignatures The list of known file signatures.
 * @param [in] num_of_signatures The number of known file signatures.
 * @return The "struct magicnumber" containing the file signature. NULL if file signature is not known.
 */
const magicnumber *headerlibrary(char *header, byte headerlen, const magicnumber filesignatures[], uint num_of_signatures);

/**
 * @breif Finds and returns a file signature from a list (array) based on the extention
 * @param [in] extentionname The file-extention without the 'dot' (ex. "png" or "txt").
 * @param [in] filesignatures The list of known file signatures.
 * @param [in] num_of_signatures The number of known file signatures.
 * @return The "struct magicnumber" containing the file signature. NULL if file signature is not found.
 */
const magicnumber *signature_by_name(char *extentionname, const magicnumber filesignatures[], uint num_of_signatures);

void retrivesettings(int argc, char **argv, settings *config);

bool compressfile(const magicnumber *filesigs, const uint sig_count, const magicnumber *comp_sig, const magicnumber *notcom_sig, char *infullpath, settings config);

bool check_compression(FILE *fpin, FILE *fpout, const magicnumber *not_compressed, char *infullpath, char *outfullpath);

char *getfilename(char *fullpath);

void print_usage();

int main(int argc, char **argv)
{
  setbuf(stderr, NULL);

  settings config;
  retrivesettings(argc, argv, &config);

  fprintf(stderr, "\nSettings: \n");
  fprintf(stderr, " - file    : %s\n", config.file);
#ifdef HAVE_DIRENT_H
  fprintf(stderr, " - w-folder: %s\n", config.watchfolder);
#endif
  fprintf(stderr, " - o-folder: %s\n", config.outputfolder);
#ifdef HAVE_DIRENT_H
  fprintf(stderr, " - watch   : %d\n", config.watch);
#endif
  fprintf(stderr, " - verbose : %d\n", config.verbose);
  fprintf(stderr, " - multicomp : %d\n\n", config.multicomp);

  magicnumber filesignatures[MAX_SIGNATURES]; /* Array of file signatures    */

  /* Load the database-file with filesignatures */
  const uint signature_count = load_magicnumbers(filesignatures);

  /* Retrieve the two special cases for the extentions "COMP" and "NCOMP" */
  const magicnumber *compressed     = signature_by_name("COMP", filesignatures, signature_count);
  const magicnumber *not_compressed = signature_by_name("NCOMP", filesignatures, signature_count);

  if(compressed == NULL || not_compressed == NULL)
  {
    fputs("Wasn't able to find a matching signature either for COMP or NCOMP, check your filetypedb.\nExiting...\n", stderr);
    exit(0);
  }

#ifdef HAVE_DIRENT_H
  if(config.watch)
  {
    DIR *pwatchdir = opendir(config.watchfolder);
    if(pwatchdir != NULL)
    {
      for(;;)
      {
        struct dirent *pent;
        while((pent = readdir(pwatchdir)) != NULL)
        {
          bool is_normal_file = true;

          /* Check if file is a "normal file", if possible */
#if defined _DIRENT_HAVE_D_TYPE
          is_normal_file = pent->d_type == DT_REG;
#elif defined HAVE_SYS_STAT_H
          char temppath[FILENAME_MAX];
          strcpy(temppath, config.watchfolder);
          strcat(temppath, pent->d_name);
          struct stat sb;
          if(stat(temppath, &sb) != -1)
            is_normal_file = S_ISREG(sb.st_mode);
#endif

          /* If name of "dirent" does not start with '.' to avoid compressing folders
           * "." (current folder) and ".." (parent folder), or hidden files.
           */
          if(pent->d_name[0] != '.' && is_normal_file)
          {
            char infullpath[FILENAME_MAX];
            strcpy(infullpath, config.watchfolder);
            strcat(infullpath, pent->d_name);
            compressfile(filesignatures, signature_count, compressed, not_compressed, infullpath, config);
            if(config.verbose)
              fprintf(stderr, "\n\n");
          }
        }
        rewinddir(pwatchdir);
      }
    }
  }
  else
  {
    compressfile(filesignatures, signature_count, compressed, not_compressed, config.file, config);
  }
#else
  compressfile(filesignatures, signature_count, compressed, not_compressed, config.file, config);
#endif

  return 0;
}

void retrivesettings(int argc, char **argv, settings *config)
{
  strcpy(config->outputfolder, "./");
  /*Gives default values to settings*/
  config->multicomp = 0;
  config->verbose = 0;
  config->watch = 0;
  strcpy(config->file, "");
  strcpy(config->watchfolder, "");

  if(argc > 1)
  {
    for(int i = 1; i < argc; ++i)
    {
      /* If there is a - followed by a letter */
      if(argv[i][0] == '-' && isalpha(argv[i][1]))
      {
        switch(argv[i][1])
        {
#ifdef HAVE_DIRENT_H
          case 'w':
            i += 1;
            strcpy(config->watchfolder, argv[i]);
            strcat(config->watchfolder, "/");
            config->watch = 1;
            break;
#endif
          case 'o':
            i += 1;
            strcpy(config->outputfolder, argv[i]);
            strcat(config->outputfolder, "/");
            break;
          case 'h':
            print_usage();
            exit(EXIT_SUCCESS);
            break;
          case 'v':
            config->verbose = 1;
            break;
          case 'm':
            config->multicomp = 1;
            break;
        }
      }
      else if(argv[i][0] == '-' && argv[i][1] == '-')
      {
        if(strcmp((argv[i] + 2), "watch") == 0)
        {
#ifdef HAVE_DIRENT_H
          i += 1;
          strcpy(config->watchfolder, argv[i]);
          strcat(config->watchfolder, "/");
          config->watch = 1;
#endif
        }
        else if(strcmp((argv[i] + 2), "output") == 0)
        {
          i += 1;
          strcpy(config->outputfolder, argv[i]);
          strcat(config->outputfolder, "/");
        }
        else if(strcmp(argv[i] + 2, "help") == 0)
        {
          print_usage();
          exit(EXIT_SUCCESS);
        }
        else if(strcmp(argv[i] + 2, "verbose") == 0)
        {
          config->verbose = 1;
        }
        else if(strcmp(argv[i] + 2, "multicomp") == 0)
        {
          config->multicomp = 1;
        }
      }
      else if(isprint(argv[i][0]))
        strcpy(config->file, argv[i]);
    }
  }
}

bool compressfile(const magicnumber *filesigs, const uint sig_count, const magicnumber *comp_sig, const magicnumber *notcom_sig, char *infullpath, settings config)
{
  char outfullpath[FILENAME_MAX];
  strcpy(outfullpath, config.outputfolder);

  /* Create a pointer to input file */
  FILE *fpin;
  if(strcmp(infullpath, "") == 0)
  {
    fpin = stdin;
    strcpy(infullpath, "out");
  }
  else
  {
    fpin = fopen(infullpath, "rb");
  }

  FILE *fpout; /* File for data output */

  const magicnumber *match = analyzedata(fpin, filesigs, sig_count, config);
  if(fpin == NULL)
    return false;

  /* If input-file needs decompression */
  if(match != NULL && match == comp_sig)
  {
    /* Remove ".COMP" file-extention (last 5 chars) from output file name */
    strcat(outfullpath, getfilename(infullpath));
    outfullpath[strlen(outfullpath) - 5] = '\0';

    /* Open a file for output */
    fpout = fopen(outfullpath, "wb");

    /* Skip the file signature for the COMP filetype and decompress the data */
    fseek(fpin, comp_sig->length, SEEK_SET);
    decompressdata(fpin, fpout);
    /* Close the input and output files */
    fclose(fpin);
    fclose(fpout);
    remove(infullpath);
  }

  /* If a file could not be compressed earlier */
  else if(match != NULL && match == notcom_sig)
  {
    /* Remove ".NCOMP" file-extention (last 5 chars) from output file name */
    strcat(outfullpath, getfilename(infullpath));
    outfullpath[strlen(outfullpath) - 6] = '\0';

    /* Open fpout and skip the file signature for the "NCOMP" filetype in fpin */

    fpout = fopen(outfullpath, "wb");
    fseek(fpin, notcom_sig->length, SEEK_SET);

    /* Write all data exept the "NCOMP" file signature from fpin to fpout */
    int ch;
    while((ch = fgetc(fpin)) != EOF)
      fputc(ch, fpout);

    /* Close the input and output files */
    fclose(fpin);
    fclose(fpout);
    remove(infullpath);
  }

  /* If file is a png */
  else if(match != NULL && strcmp(match->name, "png") == 0)
  {
    /* Add ".jpeg.NCOMP" file-extention to output file name if this is last time  */
    strcat(outfullpath, getfilename(infullpath));

    strcat(outfullpath, ".jpeg");
    if(!config.multicomp)
      strcat(outfullpath, ".NCOMP");


    if(!compressimage(infullpath, outfullpath, notcom_sig, config))
    {
      fputs("Wasn't able to compress the image", stderr);
    }

    fpout = fopen(outfullpath, "rb");

    /* If we have multicompressesion enabled and png got smaller we try and compress the now jpeg. */
    if(check_compression(fpin, fpout, notcom_sig, infullpath, outfullpath) && config.multicomp)
    {
      if(config.verbose)
        fprintf(stderr, "Try to compress jpeg\n");

      compressfile(filesigs, sig_count, comp_sig, notcom_sig, outfullpath, config);
      return true;
    }
  }

  /* If not specialcase: standard action follows below */
  else
  {
    /* Add ".COMP" file-extention to output file name */
    strcat(outfullpath, getfilename(infullpath));
    strcat(outfullpath, ".COMP");

    /* Open fpout and write "COMP" file signature to fpout */
    fpout = fopen(outfullpath, "wb");
    fwrite(comp_sig->mn, sizeof(byte), comp_sig->length, fpout);

    /* Compress data and append result to fpout */
    compressdata(fpin, fpout);

    /* Ensure the resulting output is the smaller of the (hopefully) compressed file and the original file */
    check_compression(fpin, fpout, notcom_sig, infullpath, outfullpath);
  }
  if(config.verbose)
    fprintf(stderr, "Output written to \"%s\".\n", outfullpath);

  return true;
}

const uint load_magicnumbers(magicnumber *filesignatures)
{
  bool sucess = 1; /* Variable indicating if successfully reading a line */
  uint i = 0;      /* The current index in "filesignatures"              */
  FILE *file;      /* The file containing the magic numbers              */

  /* Open database and make sure file is open */
  file = fopen("filetypedb", "rb");
  if(file == NULL)
  {
    fputs("Error while reading database for file signatures! Are you in the correct working directory?\n", stderr);
    exit(EXIT_FAILURE);
  }

  /* Start loading the filesignatures to "filesignatures" */
  fputs("Loading database of file signatures:\n", stderr);

  /* Temporary storage for the magic number as a string */
  /* Each part of the magic number is 2 chars and there is one space between
   * every part (parts - 1) and 1 null terminator. Therefore "MAX_MN_LENGTH * 3" chars */
  char *mn = (char *)malloc(3 * MAX_MN_LENGTH * sizeof(char));
  while(sucess)
  {
    /* Scan "filename", "length of magic number" and "magic number (string)" */
    sucess = (fscanf(file, " %[^:]:%hhu:%[abcdef1234567890 ],",
                     filesignatures[i].name, &(filesignatures[i].length), mn) == 3);

    /* Stop loop if no success */
    if(!sucess)
      break;

    /* "Next" is a pointer to the space after first hexadecimal number in "mn" */
    char *next = mn + 2;
    for(int n = 0; n < filesignatures[i].length; n++)
    {
      /* Read the current hexadecimal in "mn" and move pointer to end of next hexadecimal number */
      filesignatures[i].mn[n] = (byte)strtol(next - 2, &next, 16);
      next += 3;
    }

    fprintf(stderr, " :Loaded: %-5s - %s \n", filesignatures[i].name, mn);
    i++; /* Increase number of elements in "filesignatures" */
  }

  /* Close files and free memory */
  free(mn);
  fclose(file);

  /* Return the number of loaded file-signatures */
  fprintf(stderr, "Loading complete: %d elements loaded\n\n", i);
  return i;
}

const magicnumber *analyzedata(FILE *file, const magicnumber filesignatures[], uint num_of_signatures, settings config)
{
  char match[MAX_MN_LENGTH];

  /*Test if file can be opened*/
  if(file == NULL)
  {
    fputs("Issues reading the file entered.\n", stderr);
    return NULL;
  }

  /* Read (at most) the MAX_MN_LENGTH bytes of the file and null terminate */
  size_t read = fread(match, sizeof(char), MAX_MN_LENGTH, file);
  rewind(file);

  /* Check if file-header (up to first 10 bytes) matches any known file signature */
  const magicnumber *matchedheader = headerlibrary(match, read, filesignatures, num_of_signatures);

  if(config.verbose)
  {
    if(matchedheader != NULL)
      fprintf(stderr, "Found a %s signature matching in the file.\n", matchedheader->name);
    else
      fputs("No matching file signature.\n", stderr);
  }

  return matchedheader;
}

const magicnumber *headerlibrary(char *header, byte headerlen, const magicnumber filesignatures[], uint num_of_signatures)
{
  for(int i = 0; i < num_of_signatures; ++i)
  {
    /* Finds the smallest magicnumber length, either what we have in DB or what we read.*/
    uint minlength = (filesignatures[i].length < headerlen) ? filesignatures[i].length : headerlen;

    /* Compare the filesignature and the header, for the shortest of either */
    if(memcmp(header, filesignatures[i].mn, minlength) == 0)
      return filesignatures + i;
  }

  return NULL;
}

const magicnumber *signature_by_name(char *extentionname, const magicnumber filesignatures[], uint num_of_signatures)
{
  /* Search for a file signature based on the file extention */
  for(int i = 0; i < num_of_signatures; i++)
    if(strcmp(filesignatures[i].name, extentionname) == 0)
      return filesignatures + i;

  return NULL;
}

bool check_compression(FILE *fpin, FILE *fpout, const magicnumber *not_compressed, char *infullpath, char *outfullpath)
{
  /* Go to the end of the input file and output file */
  fseek(fpout, 0, SEEK_END);
  fseek(fpin, 0, SEEK_END);

  bool compressionhelped = 1;

  /* If original file is smaller than the "compressed" file */
  if(ftell(fpin) + not_compressed->length < ftell(fpout))
  {
    /* Remove the old output-file */
    fclose(fpout);
    remove(outfullpath);

    /* Construct new name for the output-file */
    uint pathlength = strlen(outfullpath) - strlen(getfilename(outfullpath));

    /* Remove file name from output path and re-insert filename with the ".NCOMP" extention */
    outfullpath[pathlength] = '\0';
    strcat(outfullpath, getfilename(infullpath));
    strcat(outfullpath, ".NCOMP");

    /* Create a new output file */
    fpout = fopen(outfullpath, "wb");
    fwrite(not_compressed->mn, sizeof(byte), not_compressed->length, fpout);

    /* Rewind fpin and write the original content to the new output file */
    rewind(fpin);
    int c;
    while((c = fgetc(fpin)) != EOF)
      fputc(c, fpout);

    compressionhelped = 0; /* Original file was smaller */
  }

  fclose(fpin);
  fclose(fpout);
  remove(infullpath);

  return compressionhelped;
}

char *getfilename(char *fullpath)
{
  char *filename = fullpath;
  char *path = fullpath;
  while((path = strchr(path + 1, '/')) != NULL)
    filename = path + 1;
  return filename;
}

void print_usage()
{
  printf("Usage: FLC [OPTIONS] [FILENAME"
#ifdef HAVE_DIRENT_H
         " (ignored and optional if -w --watch is set)"
#endif
         "]\n"
         "[OPTIONS]\n"
#ifdef HAVE_DIRENT_H
         "  -w  --watch [PATH]   Watches a directory for changes.\n"
#endif
         "  -o  --output [PATH]  The path whereto the output should be written (defult ./).\n"
         "  -m  --multialg       If set the progralm will try to compress a png file twice.\n"
         "  -v  --verbose        Program will run in verbose mode.\n"
         "  -h  --help           Print this usage message.\n");
}
