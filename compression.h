#pragma once


#include <stdio.h>
#include <stdbool.h>

#include <zlib.h>

#include "settings.h"

#include "stb_image.h"
#include "tiny_jpeg.h"

#define CHUNK 16384
#define LEVEL Z_BEST_COMPRESSION
#define JPEG_QUALITY 1

int compressdata(FILE *fpin, FILE *fpout);
int decompressdata(FILE *fpin, FILE *fpout);
bool compressimage(char *inputname, char *outputname, const magicnumber *sig, settings config);
