#include "compression.h"

int compressdata(FILE *fpin, FILE *fpout) {
    int ret, flush;
    unsigned int have; /* The number of bytes to write to output file */
    z_stream stream;
    unsigned char in[CHUNK];  /* "CHUNK" from "compression.h" */
    unsigned char out[CHUNK];

    /* allocate deflate state */
    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;
    ret = deflateInit(&stream, LEVEL); /* "LEVEL" from "compression.h" */
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        stream.avail_in = fread(in, 1, CHUNK, fpin);
        if (ferror(fpin)) {
            deflateEnd(&stream);
            return Z_ERRNO;
        }
        flush = feof(fpin) ? Z_FINISH : Z_NO_FLUSH;
        stream.next_in = in;

        /* run deflate() on input until output buffer not full, finish
         *           compression if all of source has been read in */
        do {
            stream.avail_out = CHUNK;
            stream.next_out = out;
            deflate(&stream, flush);
            have = CHUNK - stream.avail_out;
            if (fwrite(out, 1, have, fpout) != have || ferror(fpout)) {
                deflateEnd(&stream);
                return Z_ERRNO;
            }
        } while (stream.avail_out == 0);

        /* done when last data in file processed */
    } while (flush != Z_FINISH);

    /* clean up and return */
    deflateEnd(&stream);
    return Z_OK;
}

int decompressdata(FILE *fpin, FILE *fpout) {
    int ret;
    unsigned int have; /* The number of bytes to write to output file */
    z_stream stream;
    unsigned char in[CHUNK];  /* "CHUNK" from "compression.h" */
    unsigned char out[CHUNK];

    /* allocate inflate state */
    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;
    stream.avail_in = 0;
    stream.next_in = Z_NULL;
    ret = inflateInit(&stream);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
        stream.avail_in = fread(in, 1, CHUNK, fpin);
        if (ferror(fpin)) {
            (void)inflateEnd(&stream);
            return Z_ERRNO;
        }
        if (stream.avail_in == 0)
            break;
        stream.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            stream.avail_out = CHUNK;
            stream.next_out = out;
            ret = inflate(&stream, Z_NO_FLUSH);
            switch (ret) {
                case Z_NEED_DICT:
                    ret = Z_DATA_ERROR;     /* and fall through */
                case Z_DATA_ERROR:
                case Z_MEM_ERROR:
                    inflateEnd(&stream);
                    return ret;
            }
            have = CHUNK - stream.avail_out;
            if (fwrite(out, 1, have, fpout) != have || ferror(fpout)) {
                inflateEnd(&stream);
                return Z_ERRNO;
            }
        } while (stream.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    inflateEnd(&stream);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

bool compressimage(char *inputname, char *outputname, const magicnumber *sig, settings config)
{
    int width, height, num_components;
    unsigned char* image = stbi_load(inputname, &width, &height, &num_components, 0);
    if ( !image ) {
        fputs("Could not find file\n", stderr);
        return 0;
    }
    FILE* fd = fopen(outputname, "wb");
    if(fd == NULL)
    {
        if(config.verbose)
          fputs("Couldn't open image destination.", stderr);
      return false;
    }
    if(!config.multicomp) {
      if(!fwrite(sig->mn, sizeof(byte), sig->length, fd))
        fputs("Could not write magicnumber to start of file.",stderr);
    }
    bool ret = tje_encode_with_func(tjei_stdlib_func, fd, JPEG_QUALITY, width, height, num_components, image);
    fclose(fd);
    return ret;
}
