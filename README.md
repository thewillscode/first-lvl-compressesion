Seal-Time Compression
=====================
### Contents
1. [Introduction](#markdown-header-introduction)
2. [License](#markdown-header-license)
3. [Usage](#markdown-header-usage)
4. [Installation](#markdown-header-installation)
	1. [Method 1](#markdown-header-method-1-cmake)
	2. [Method 2](#markdown-header-method-2-manual-configuration)
5. [Additional documentation](#markdown-header-additional-documentation)

Introduction
------------
This is a program developed specifically for usage in low bandwidth areas of the globe (with focus on Greenland). The program uses ZLIB which is required for the compression. If the system wherein this program is implemented, needs to use a watch directory, dirent.h is also required. It is written with the requirement of C99 compatible compiler.

License
-------
See [license file](/LICENSE) in the root of the project. Other licenses may apply to some files. If no license in the file header the [license file](/LICENSE) apply.

Usage
-----
STC [OPTIONS] [FILENAME(ignored and optional if -w --watch is set)]

**OPTIONS**

* -w  --watch [PATH]   Watches a directory for changes.
* -o  --output [PATH]  The path whereto the output should be written (defult ./).
* -m  --multialg       If set the progralm will try to compress a png file twice.
* -v  --verbose        Program will run in verbose mode.
* -h  --help           Print this usage message.

* [PATH] must be in unix style with forwardsslashes


Installation
------------
Required packages:

* **ZLIB**
	* **Windows**
		* If using **Cygwin**, install the package "
		**``mingw64-i686-zlib``** " or " **``mingw64-x86_64-zlib``** "
        depending on the architecture, the build should target.
    	* If using **MSYS2**, install the package "
		**``mingw-w64-i686-zlib``** " or " **``mingw-w64-x86_64-zlib``** "
        depending on the architecture, the build should target.
    	* It's also possible to manually download the source from
    	[**ZLIB**](https://zlib.net/). To compile this you would
    	have to follow their provided instructions.
	* **Debian based linux distrobutions (Ubuntu, Linux Mint, etc.):**
		* Install the package " **``zlib1g-dev``** ".
	* **RHEL, CentOS and Fedora**
		* Install the package " **``zlib-devel``** ".
	* **OSX**
		* **Untested**
* **C99 compiler**
    * **GCC** - C Compiler
    * **Clang/LLVM** - C compiler
    * **MSVC** *(untested)*
* **A cmake compatible build system**
	* **Unix Makefiles**
	* **Ninja**
	* **NMake**
	* **Etc...** Read the **cmake documentation**.

The program supports 2 installation methods.

### Method 1 - **cmake**
In this method **cmake** is required and must be added to your
systems **PATH** variable (accessible from your terminal emulator).
You would also need a build system such as Unix Makefiles.
Other build systems are supported, read the **cmake documentation**.

1. Open your terminal/console.
2. Change directory (cd) to the project directory.
3. Make a directory with the name " **``build``** " (``mkdir build``).
4. Now change directory to the new **build** folder (``cd build``).
5. Run the command " **``cmake ..``** ".
    * You might need to specify a specific generator
    such as the **Unix Makefiles**. To do this run **cmake**
    with the option " **``-G "Unix Makefiles"``** ".
    Read the **cmake documentation** for more help.
    * Please note that in case of a failure during this step you might
    need to investigate why it was unsucessful in building the
    program youself. It's most likely because it wasn't able to find
    **ZLIB**, and if that's the case, you would have to specify where
    **cmake** should find **ZLIB** manually. To do so run **cmake**
    with the options " **``-DZLIB_LIBRARY=LIBPATH``** " and
    **``-DZLIB_INCLUDE_DIR=INCLUDEPATH``** ". **cmake** would
    also notify you in case it wasn't able to find **dirent.h**
    which would render the watch directory option disabled.
6. This next step is dependend on the specified build system,
but in this case the focus will be on **Unix Makefiles**. To
use compile the program now all that is needed is to run the
command " **``make``** ". Remember that the usual **make** are still
usable. E.g. " **``-j4``** ".
7. **Optional**
This step is as the previous step dependend on the build system
cmake generated files for. Though continuing with **Unix Makefiles**
as an example, to install the program system wide run the command
" **``make install``** ". This will install the program in a specified
folder and if not a default folder. This is very much system dependend.
Also note that on most linux systems (depending on the setup) this
would require it to be run with root privileges (if the install target
is / or paths owned by root).
8. Please remember that a file, called **filetypedb** is needed in the current
  working directory of the program.

### Method 2 - **Manual configuration**
In this method the configuration of the project will be done manually.
This means that you manually would have to ensure certain headers are
available for the compiler to use. Also depending on this you would
have to write a **configuration.h** file, using **configuration.h.in**
as a template.

1. Open your terminal/console.
2. Change directory (" **``cd``** ") to the project directory.
3. Now copy the **``configuration.h.in``** to a file called
	**``configuration.h``** (``cp configuration.h.in configuration.h``).
4. Open the **``configuration.h``** file in a text editor and change " **``#cmakedefine``** "
	to " **``#define``** " if the corresponding header file is accessable by the
    compiler, otherwise delete/remove the line.
5. Make a directory with the name " **``build``** " (``mkdir build``).
6. Now change directory to the new **build** folder (``cd build``).
7. Depending on your compilerm, different compiler options should be defined, but
	using gcc as an example you would have to run the command
    " **``gcc -o STC ../main.c ../compression.c -lz -lm``** ". **Note** that if your
    installation of **ZLIB** isn't located in default system locations. You
    would have to call **gcc** with the argument " **``-I INCLUDEPATH``** " and
    **``-L LIBPATH``** ".
8. Please remember that a file, called **filetypedb** is needed in the current
  working directory of the program.

Additional documentation
------------------------
The program has been written with doxygen documentation. This enables you
to generate a documentation with doxygen. This can be outputted in
multiple formats. It is also possible to just read the commments with a
normal text editor, but this may be more difficult. It should also be noted
that not every function is documented, therefore enabling the feature to
document all entries may help.
