#pragma once

#define MAX_MN_LENGTH 10  /* Max magic number length             */

typedef unsigned char byte;
typedef unsigned int uint;

/**
 * @brief A struct containing information about a magicnumber.
 */
typedef struct
{
  char name[8]; /**< The name of the filetype this magicnumber contains. */
  byte mn[MAX_MN_LENGTH]; /**< The specific magicnumber. */
  byte length; /**< The length of the specific magicnumber. */
} magicnumber;

/**
 * @brief A struct containing the settings of the program.
 */
typedef struct
{
  char file[FILENAME_MAX]; /**< The filename which should be compressed. */
  char watchfolder[FILENAME_MAX]; /**< The watch folder where from the files should be compressed. */
  char outputfolder[FILENAME_MAX]; /**< The output folder of the compressed files. */
  byte watch: 1; /**< Whether the program should watch a watch folder or not. */
  byte verbose: 1; /**< Whether the program should be noisy. */
  byte multicomp: 1; /**< Whether the program should compress with multiple algorithms if possible. */
} settings;
